from pysnmp.entity.rfc3413.oneliner import cmdgen
from pysnmp.proto import rfc1902
import random, sys

class SNMPException (Exception): pass

cmdGen = cmdgen.CommandGenerator()

# These get set by the caller
taskNum = None
community = None
target = None
tftp_server = None
conf_file = None

mibBase = '1.3.6.1.4.1.9.9.96.1.1.1.1'
ConfigCopyProtocol = '2'
SourceFileType = '3'
DestinationFileType = '4'
ServerAddress = '5'
CopyFilename = '6'
CopyStatus = '14'
CopyState = '10'
iState = {1: 'waiting', 2: 'running', 3: 'successful', 4: 'failed'}
sState = { v : k for k, v in iState.items () }

def setSNMPVar(command, value, datatype):
	global taskNum, community, target, tftp_server, conf_file

	if datatype == 'Integer':
		val = rfc1902.Integer(value)
	elif datatype == 'IpAddress':
		val = rfc1902.IpAddress(value)
	elif datatype == 'OctetString':
		val = rfc1902.OctetString(value)
	else:
		raise SNMPException('setSNMPVar: Unknown datatype "{0}", exiting.'.format(datatype))

	errorIndication, errorStatus, errorIndex, varBinds = cmdGen.setCmd(
		community, target, ('{0}.{1}.{2}'.format(mibBase, command, taskNum), val)
	)
	if errorIndication:
		raise SNMPException(errorIndication)
	else:
		if errorStatus:
			raise SNMPException('%s at %s' % ( errorStatus.prettyPrint(), errorIndex and varBinds[int(errorIndex)-1] or '?'))

def readState():
	global taskNum, community, target, tftp_server, conf_file

	errorIndication, errorStatus, errorIndex, varBinds = cmdGen.getCmd(
		community, target, '{0}.{1}.{2}'.format(mibBase, CopyState, taskNum)
	)

	# Check for errors and return results
	if errorIndication:
		raise SNMPException(errorIndication)
	else:
		if errorStatus:
			raise SNMPException('%s at %s' % ( errorStatus.prettyPrint(), errorIndex and varBinds[int(errorIndex)-1] or '?'))
		else:
			for name, val in varBinds:
				return val.prettyPrint()

	return None

def reload_router(a_target, a_community, a_tftp_server, a_conf_file):
	global taskNum, community, target, tftp_server, conf_file
	
	taskNum = str(random.randrange(100, 5000))
	community = cmdgen.CommunityData(a_community)
	target = cmdgen.UdpTransportTarget((a_target, 161))
	tftp_server = a_tftp_server
	conf_file = a_conf_file

	setSNMPVar(ConfigCopyProtocol, 1, 'Integer')
	setSNMPVar(SourceFileType, 1, 'Integer')
	setSNMPVar(DestinationFileType, 4, 'Integer')
	setSNMPVar(ServerAddress, tftp_server, 'IpAddress')
	setSNMPVar(CopyFilename, conf_file, 'OctetString')
	setSNMPVar(CopyStatus, 1, 'Integer')

	old_state = 99
	while True:
		state = int(readState())
		if state != old_state:
			old_state = state
			print('\n{0}'.format(iState[state]), end='')
		else:
			print('.', end='')
		sys.stdout.flush()

		if state == sState['failed'] or state == sState['successful']:
			print()
			break

	setSNMPVar(CopyStatus, 6, 'Integer')
