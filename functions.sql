CREATE OR REPLACE FUNCTION tunnels_table_changed() RETURNS TRIGGER AS $function$
	DECLARE
		notify_string VARCHAR DEFAULT '';
		old_str VARCHAR DEFAULT '';
		new_str VARCHAR DEFAULT '';
	BEGIN
		IF (TG_OP = 'INSERT') THEN
			SELECT INTO new_str tunnel_to_json(NEW.tunnel_idx, NEW.ip, NEW.dynamic_ip, NEW.ip_class, NEW.hostname, NEW.description, NEW.source, NEW.cost, NEW.email, NEW.snmp_community, NEW.tunnel_type, NEW.topology_type);
			notify_string := '{"action": "INSERT", "NEW": ' || new_str || '}';
		ELSIF (TG_OP = 'UPDATE') THEN
			SELECT INTO new_str tunnel_to_json(NEW.tunnel_idx, NEW.ip, NEW.dynamic_ip, NEW.ip_class, NEW.hostname, NEW.description, NEW.source, NEW.cost, NEW.email, NEW.snmp_community, NEW.tunnel_type, NEW.topology_type);
			SELECT INTO old_str tunnel_to_json(OLD.tunnel_idx, OLD.ip, OLD.dynamic_ip, OLD.ip_class, OLD.hostname, OLD.description, OLD.source, OLD.cost, OLD.email, OLD.snmp_community, OLD.tunnel_type, OLD.topology_type);
			notify_string := '{"action": "UPDATE", "NEW": ' || new_str || ', "OLD": ' || old_str || '}';
		ELSIF (TG_OP = 'DELETE') THEN
			SELECT INTO old_str tunnel_to_json(OLD.tunnel_idx, OLD.ip, OLD.dynamic_ip, OLD.ip_class, OLD.hostname, OLD.description, OLD.source, OLD.cost, OLD.email, OLD.snmp_community, OLD.tunnel_type, OLD.topology_type);
			notify_string := '{"action": "DELETE", "OLD": ' || old_str || '}';
		END IF;

		IF notify_string != '' THEN
			PERFORM pg_notify('tunnels', notify_string);
		ELSE
			PERFORM pg_notify('tunnels', '{"action": "ERROR", "message": "huh? notify_string is empty"}');
		END IF;

		RETURN NULL;
	END;
$function$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION manual_push(reason VARCHAR) RETURNS VARCHAR AS $function$
	DECLARE
		notify_string VARCHAR;
	BEGIN
		notify_string := '{"action": "MANUAL", "reason": "' || reason || '"}';
		PERFORM pg_notify('tunnels', notify_string);
		RETURN notify_string;
	END;
$function$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION tunnel_to_json(tunnel_idx INTEGER, ip VARCHAR, dynamic_ip BOOLEAN, ip_class INTEGER, hostname VARCHAR, description VARCHAR, source VARCHAR, cost INTEGER, email VARCHAR, snmp_community VARCHAR, tunnel_type VARCHAR, topology_type VARCHAR) RETURNS VARCHAR AS $function$
	BEGIN
		RETURN '{"tunnel_idx": ' || tunnel_idx || ', "ip": "' || ip || '", "dynamic_ip": "' || dynamic_ip || '", "ip_class": ' || ip_class || ', "hostname": "' || hostname || '", "description": "' || description || '", "source": "' || source || '", "cost": ' || cost || ', "email": "' || email || '", "snmp_community": "' || snmp_community || '", "tunnel_type": "' || tunnel_type || '", "topology_type": "' || topology_type || '"}';
	END;
$function$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION tunnel_is_IPSec(tun1 INTEGER, tun2 INTEGER) RETURNS BOOLEAN AS $function$
	DECLARE
		tun1r RECORD;
		tun2r RECORD;
	BEGIN
		SELECT INTO tun1r * FROM tunnels WHERE tunnel_idx = tun1 AND tunnel_type = 'IPSec';
		SELECT INTO tun2r * FROM tunnels WHERE tunnel_idx = tun2 AND tunnel_type = 'IPSec';
		IF tun1r IS NOT NULL AND tun2r IS NOT NULL THEN
			RETURN TRUE;
		END IF;
		RETURN FALSE;
	END;
$function$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION tunnel_gets_created(tun1 INTEGER, tun2 INTEGER) RETURNS BOOLEAN AS $function$
	DECLARE
		tun1r VARCHAR;
		tun2r VARCHAR;
	BEGIN
		SELECT INTO tun1r topology_type FROM tunnels WHERE tunnel_idx = tun1;
		SELECT INTO tun2r topology_type FROM tunnels WHERE tunnel_idx = tun2;
		IF tun1r = 'mesh' AND tun2r = 'mesh' THEN
			RETURN TRUE;
		ELSIF tun1r = 'mesh' AND tun2r = 'hub' THEN
			RETURN TRUE;
		ELSIF tun1r = 'hub' AND tun2r = 'mesh' THEN
			RETURN TRUE;
		ELSIF tun1r = 'hub' AND tun2r = 'hub' THEN
			RETURN TRUE;
		ELSIF tun1r = 'spoke' AND tun2r = 'hub' THEN
			RETURN TRUE;
		ELSIF tun1r = 'hub' AND tun2r = 'spoke' THEN
			RETURN TRUE;
		END IF;
		RETURN FALSE;
	END;
$function$ LANGUAGE plpgsql;
