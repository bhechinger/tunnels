CREATE TABLE tunnels (
	tunnel_idx SERIAL NOT NULL PRIMARY KEY,
	ip VARCHAR NOT NULL,
	dynamic_ip BOOLEAN NOT NULL DEFAULT(FALSE),
	ip_class INT NOT NULL DEFAULT(4),
	hostname VARCHAR NOT NULL,
	description VARCHAR NOT NULL,
	source VARCHAR NOT NULL,
	cost INT NOT NULL DEFAULT(10),
	email VARCHAR NOT NULL DEFAULT('none'),
	snmp_community VARCHAR NOT NULL DEFAULT('none'),
	tunnel_type VARCHAR NOT NULL DEFAULT('GRE'),
	topology_type VARCHAR NOT NULL DEFAULT('mesh'),
	CONSTRAINT "tunnel_type can only be GRE or IPSec" CHECK (tunnel_type = 'GRE' OR tunnel_type = 'IPSec'),
	CONSTRAINT "topology_type can only be mesh, hub or spoke" CHECK (topology_type = 'mesh' OR topology_type = 'hub' OR topology_type = 'spoke'),
	CONSTRAINT "ip_class is 4 or 6" CHECK (ip_class = 4 OR ip_class = 6),
	CONSTRAINT "tunnel index must be higher than 50" CHECK (tunnel_idx >= 50),
	UNIQUE (hostname, ip_class)
);

CREATE TABLE config_info (
	ip_class INT NOT NULL,
	db_version INT NOT NULL,
	CONSTRAINT "ip_class is 4 or 6" CHECK (ip_class = 4 OR ip_class = 6),
	UNIQUE (ip_class)
);

CREATE TABLE exclusion_list (
	idx SERIAL NOT NULL PRIMARY KEY,
	tun1 SERIAL NOT NULL,
	tun2 SERIAL NOT NULL,
	CONSTRAINT "tun1 needs to be larger than tun2" CHECK (tun1 > tun2),
	UNIQUE(tun1, tun2)
);
