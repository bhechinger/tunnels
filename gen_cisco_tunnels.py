#!/usr/local/bin/python3
from reload_router import reload_router, SNMPException
from email.mime.text import MIMEText
import subprocess, collections, socket, select, random, json, sys, argparse
import psycopg2, psycopg2.extensions
import smtplib

parser = argparse.ArgumentParser(description='Listen to a database channel and create new tunnel configs when the DB changes.')
parser.add_argument("-c", "--config_file", help="configuration file to use. Uses tunnels.conf from the current working directory if not specified.")
parser.add_argument("-t", "--test", help="Enable TEST mode for debugging", action="store_true")
parser.add_argument("-n", "--no_email", help="Don't send notification emails", action="store_true")
args = parser.parse_args()

config_file = "tunnels.conf"
if args.config_file:
	config_file = args.config_file

try:
	with open(config_file, 'r') as cfile:
		config = json.loads(cfile.read())
except IOError as oops:
	print("""\n{0}\n""".format(oops))
	sys.exit()

# Globals
no_email = args.no_email
test = args.test

if test:
	test_dir = 'test/'
	test_sub = '[TEST] '
else:
	test_dir = ''
	test_sub = ''

# these should be moved to the config file
data_dir = '/tftpboot/{0}'.format(test_dir)
tftp_server = '216.15.64.181'
subject = '{0}HECnet Cisco Router Config for'.format(test_sub)
from_email = 'HECnet Cisco Config Tool <hecnet@4amlunch.net>'

channel_name = 'tunnels'
try:
	if config['channel_name']:
		channel_name = config['channel_name']
except KeyError:
	pass

push_list = []

# Setup DB connection
DSN = "dbname={0} user={1} password={2}".format(config['dbname'], config['dbuser'], config['dbpass'])
try:
	if config['dbhost']:
		DSN += " host={0}".format(config['dbhost'])
except KeyError:
	pass

try:
	if config['dbport']:
		DSN += " port={0}".format(config['dbport'])
except KeyError:
	pass

db_conn = psycopg2.connect(DSN)
db_conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

# fetch and update version
def get_version(ip_class):
	global db_conn

	version_cursor = db_conn.cursor()
	version_cursor.execute("""SELECT db_version FROM config_info WHERE ip_class = %s;""", (ip_class,))
	version = version_cursor.fetchone()[0] + 1
	version_cursor.execute("""UPDATE config_info SET db_version = %s WHERE ip_class = %s;""", (version, ip_class))
	version_cursor.close()
	##db_conn.commit()
	return version

def update_version_file(version, ip_class):
	global data_dir

	# write the latest version to the file so people can check
	vfile = open('{0}db_version-ipv{1}'.format(data_dir, ip_class), 'w')
	vfile.write('{0}\n'.format(version))
	vfile.close

def fetch_tunnels(ip_class):
	global db_conn

	fetch_cursor = db_conn.cursor()
	fetch_cursor.execute("""SELECT * FROM tunnels WHERE ip_class = %s;""", (ip_class,))

	# Fetch column names into a namedtuple for reference
	columns = [ desc[0] for desc in fetch_cursor.description ]
	Column = collections.namedtuple ("Column", columns)

	# put the db info into a dictionary for easier processing
	tunnels = [ Column (*row) for row in fetch_cursor ]

	fetch_cursor.close()
	return tunnels

def tunnel_order(tun1, tun2):
	if tun1 > tun2:
		return tun1, tun2
	else:
		return tun2, tun1

def tunnel_not_excluded(tun1, tun2):
	status = False
	cursor = db_conn.cursor()
	cursor.execute("""SELECT * FROM exclusion_list WHERE tun1 = %s and tun2 = %s;""", (tunnel_order(tun1.tunnel_idx, tun2.tunnel_idx)))
	if cursor.rowcount == 0:
		status = True

	cursor.close()
	return status

def tunnel_is_IPSec(tun1, tun2):
	cursor = db_conn.cursor()
	cursor.execute("""SELECT * FROM tunnel_is_IPSec(%s, %s);""", (tun1.tunnel_idx, tun2.tunnel_idx))
	result = cursor.fetchone()[0]
	cursor.close()
	return result

def tunnel_gets_created(tun1, tun2):
	if tun1 == tun2:
		return False
	if tunnel_not_excluded(tun1, tun2):
		cursor = db_conn.cursor()
		cursor.execute("""SELECT * FROM tunnel_gets_created(%s, %s);""", (tun1.tunnel_idx, tun2.tunnel_idx))
		result = cursor.fetchone()[0]
		cursor.close()
		return result
	return False

def tunnel_data_valid(tunnels):
	# The following data is required
	for tunnel in tunnels:
		if not tunnel.tunnel_idx:
			return False
		if not tunnel.description:
			return False
		if not tunnel.cost:
			return False
		if not tunnel.source:
			return False
		if not tunnel.ip:
			return False

	return True

# Generate configs
def generate_configs(tunnels, version, ip_class, deleted):
	global data_dir, tftp_server, push_list

	# This gets weird on a delete for some reason so we force it to be empty
	push_list = []

	ip_list = open('{0}ip_list'.format(data_dir), 'w')

	for tun in tunnels:
		# create a file to stash things in
		conf_file = 'tunnel-{0}-ipv{1}.txt'.format(tun.hostname, ip_class)
		cfile = open('{0}{1}'.format(data_dir, conf_file), 'w+')
		cfile.write("!\n! Router config for {0}\n!\n".format(tun.hostname))

		if deleted:
			cfile.write("no interface Tunnel{0}\n".format(deleted))

		for tunnel in tunnels:
			if tunnel_gets_created(tun, tunnel):
				if tunnel_is_IPSec(tun, tunnel):
					print('config is IPSec for {0} {1}'.format(tun.tunnel_idx, tunnel.tunnel_idx))
				else:
					cfile.write("interface Tunnel{0}\n".format(tunnel.tunnel_idx))
					cfile.write("  description HECnet tunnel for {0} [Version:{1}]\n".format(tunnel.description, str(version)))
					cfile.write("  no ip address\n")
					cfile.write("  decnet cost {0}\n".format(tunnel.cost))
					cfile.write("  tunnel source {0}\n".format(tun.source))
					cfile.write("  tunnel destination {0}\n".format(tunnel.ip))
					cfile.write("  tunnel path-mtu-discovery\n")

		cfile.write("end\n")
		cfile.close()
		ip_list.write("{0}\n".format(tun.ip))

		# generate reload router config list
		if tun.hostname == 'bart.4amlunch.net':
			tmp_tftp_server = '10.42.2.9'
		else:
			tmp_tftp_server = tftp_server

		push_list.append({"tunnel_idx": tun.tunnel_idx, "tftp_server": tmp_tftp_server, "conf_file": conf_file})

	ip_list.close()
	return

# Send email
def send_emails(tunnels, reason):
	global no_email, from_email, data_dir

	if not no_email:
		for recipient in push_list:
			tun = get_tunnel(tunnels, recipient['tunnel_idx'])
			if tun:
				if tun.email != 'none':
					cfile = open('{0}{1}'.format(data_dir, recipient['conf_file']))
					print("Sending email to {0}".format(tun.email))
					msg = MIMEText('{0}\nTFTP Info: tftp://{1}/{2}\n\nReason for change: {3}\n'.format(cfile.read(), recipient['tftp_server'], recipient['conf_file'], reason))
					cfile.close()

					msg['Subject'] = '{0} {1}'.format(subject, tun.hostname)
					msg['From'] = from_email
					msg['To'] = tun.email

					s = smtplib.SMTP('localhost')
					s.sendmail(from_email, tun.email, msg.as_string())
					s.quit()

# get a single tunnel from the list
def get_tunnel(tunnels, tunnel_idx):
	for tun in tunnels:
		if tunnel_idx == tun.tunnel_idx:
			return tun
	return None

# reload router configs
def reload_routers():
	global test, push_list

	for routers in push_list:
		tun = get_tunnel(tunnels, routers['tunnel_idx'])
		if tun:
			if tun.snmp_community != 'none':
				print('Reloading router: {0} {1} {2} {3}'.format(tun.ip, tun.snmp_community, routers['tftp_server'], routers['conf_file']))
				if not test:
					try:
						reload_router(tun.ip, tun.snmp_community, routers['tftp_server'], routers['conf_file'])
					except SNMPException as oops:
						print('Error reloading router: {0} :: {1}'.format(tun.hostname, oops))

def has_new(action):
	if action == 'INSERT' or action == 'UPDATE':
		return True
	return False

def has_old(action):
	if action == 'DELETE':
		return True
	return False

def is_manual(action):
	if action == 'MANUAL':
		return True
	return False

def get_ip_class(payload):
	if has_new(payload['action']):
		ip_class = payload['NEW']['ip_class']
	elif has_old(payload['action']):
		ip_class = payload['OLD']['ip_class']
	elif is_manual(payload['action']):
		# temp hack
		ip_class = 4;
	else:
		print('Invalid action {0}'.format(payload['action']))

	return ip_class

def get_reason(payload):
	if payload['action'] == 'INSERT':
		return 'Added Tunnel{0}: {1}'.format(payload['NEW']['tunnel_idx'], payload['NEW']['description']), None
	elif payload['action'] == 'UPDATE':
		reason = []

		for key in payload['NEW'].keys():
			if payload['NEW'][key] != payload['OLD'][key]:
				reason.append('\n\t"{0}" changed from "{1}" to "{2}"'.format(key, payload['OLD'][key], payload['NEW'][key]))

		s = ''.join(reason)
		if s:
			return 'Modified Tunnel{0}: {1}{2}'.format(payload['OLD']['tunnel_idx'], payload['OLD']['description'], s), None
		else:
			return None, None
	elif payload['action'] == 'DELETE':
		return 'Deleted Tunnel{0}: {1}'.format(payload['OLD']['tunnel_idx'], payload['OLD']['description']), payload['OLD']['tunnel_idx']
	elif payload['action'] == 'MANUAL':
		return 'Manual push: {0}'.format(payload['reason']), None
	else:
		return 'Unknown action: {0}'.format(payload['action'])

######################
# Let's get to work! #
######################

listen_cursor = db_conn.cursor()
listen_cursor.execute("""LISTEN {0};""".format(channel_name))

print("\n\tUsing config file: {0}".format(config_file))
print("\tConnected to database: {0}".format(config['dbname']))
print("\tWaiting for notifications on channel '{0}'\n".format(channel_name))
while 1:
	if not select.select([db_conn],[],[],5) == ([],[],[]):
		db_conn.poll()
		while db_conn.notifies:
			notify = db_conn.notifies.pop()
			print('Got NOTIFY: PID={0}, CHANNEL={1}, PAYLOAD={2}'.format(notify.pid, notify.channel, notify.payload))
			payload = json.loads(notify.payload)

			if payload['action'] == 'ERROR':
				print('SQL Trigger returned an error: {0}'.format(payload['message']))
				continue

			# get_reason() needs to be called early as it has a side effect of populating the deleted list
			reason, deleted = get_reason(payload)
			ip_class = get_ip_class(payload)
			version = get_version(ip_class)
			tunnels = fetch_tunnels(ip_class)

			if tunnel_data_valid(tunnels):

				update_version_file(version, ip_class)
				generate_configs(tunnels, version, ip_class, deleted)

				if reason:
					send_emails(tunnels, reason)

					if not test:
						# update firewall with new IPs
						subprocess.call(['sudo', '/home/hecnet/refresh_table'])

					reload_routers()
				else:
					print("Nothing appears to actually have changed!")

				print('Completed NOTIFY: PID={0}, CHANNEL={1}, PAYLOAD={2}'.format(notify.pid, notify.channel, notify.payload))
			else:
				print('Tunnel data invalid, check database!');
