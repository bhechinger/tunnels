--- CREATE TRIGGER tunnel_script_trig
--- AFTER INSERT OR UPDATE ON tunnels
--- FOR EACH STATEMENT EXECUTE PROCEDURE tunnel_script();

CREATE TRIGGER tunnels_changed_trigger
AFTER INSERT OR UPDATE OR DELETE ON tunnels
FOR EACH ROW EXECUTE PROCEDURE tunnels_table_changed();
