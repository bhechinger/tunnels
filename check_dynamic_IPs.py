#!/usr/local/bin/python3
import socket, collections
import psycopg2

conn = psycopg2.connect("dbname=hecnet user=hecnet password=Y#Dh35")
cur = conn.cursor()
cur2 = conn.cursor()

cur.execute("""SELECT * FROM tunnels WHERE dynamic_ip = 't';""")
# Fetch column names into a namedtuple for reference
columns = [ desc[0] for desc in cur.description ]
Column = collections.namedtuple ("Column", columns)

# put the db info into a dictionary for easier processing
tunnels = [ Column (*row) for row in cur ]

for tun in tunnels:
	old_ip = tun.ip
	new_ip = socket.gethostbyname(tun.hostname)
	if new_ip != old_ip:
		cur2.execute("""UPDATE tunnels SET ip = %s WHERE tunnel_idx = %s;""", (new_ip, tun.tunnel_idx))
		conn.commit()
		print("Updated: {0}".format(tun.hostname))

# Close communication with the database
cur.close()
cur2.close()
conn.close()
